# NetAcademia-Android-masodik

Videó során használt linkek

## Global
* Android dashboard https://developer.android.com/about/dashboards/
* Activity lifecycle https://www.javatpoint.com/images/androidimages/Android-Activity-Lifecycle.png
* Speed comparsion https://notes.devlabs.bg/realm-objectbox-or-room-which-one-is-for-you-3a552234fd6e
* Szintén https://www.netguru.co/codestories/realm-vs-objectbox-comparison

## Libek
* Support libek https://developer.android.com/topic/libraries/support-library/packages
* Butterknife https://github.com/JakeWharton/butterknife
* ObjectBox https://docs.objectbox.io/getting-started

## Guide oldalak

* Spinner töltés https://v4all123.blogspot.com/2017/11/simple-example-of-using-spinner-in.html