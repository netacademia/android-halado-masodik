package hu.netacademia.androidhalado02.cache.child

import android.content.Context
import hu.netacademia.androidhalado02.NetAcademiaApplication
import hu.netacademia.androidhalado02.cache.BaseCache
import hu.netacademia.androidhalado02.entities.Car
import hu.netacademia.androidhalado02.entities.Manufacturer

class CarCache(context: Context) : BaseCache<Car>(
        (context.applicationContext as NetAcademiaApplication).boxStore.boxFor(Car::class.java)
)