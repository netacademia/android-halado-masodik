package hu.netacademia.androidhalado02.cache.child

import android.content.Context
import hu.netacademia.androidhalado02.NetAcademiaApplication
import hu.netacademia.androidhalado02.cache.BaseCache
import hu.netacademia.androidhalado02.entities.Type

class TypeCache(context: Context) : BaseCache<Type>(
        (context.applicationContext as NetAcademiaApplication).boxStore.boxFor(Type::class.java)
)