package hu.netacademia.androidhalado02.cache.child

import android.content.Context
import hu.netacademia.androidhalado02.NetAcademiaApplication
import hu.netacademia.androidhalado02.cache.BaseCache
import hu.netacademia.androidhalado02.entities.Manufacturer


class ManufacturerCache(context: Context) : BaseCache<Manufacturer>(
        (context.applicationContext as NetAcademiaApplication).boxStore.boxFor(Manufacturer::class.java)
)