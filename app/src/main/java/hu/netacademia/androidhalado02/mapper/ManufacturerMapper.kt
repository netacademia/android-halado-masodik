package hu.netacademia.androidhalado02.mapper

import hu.netacademia.androidhalado02.entities.Manufacturer

class ManufacturerMapper : Mapper<Manufacturer> {

    override fun transformToStringList(target: List<Manufacturer>): List<String> {
        return target.map { manufacturer ->
            manufacturer.name
        }
    }
}