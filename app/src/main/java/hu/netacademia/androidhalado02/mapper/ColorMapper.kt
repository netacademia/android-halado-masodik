package hu.netacademia.androidhalado02.mapper

import hu.netacademia.androidhalado02.entities.Car

class ColorMapper : Mapper<Car> {

    override fun transformToStringList(target: List<Car>): List<String> {
        return target.map { car ->
            car.color
        }
    }
}