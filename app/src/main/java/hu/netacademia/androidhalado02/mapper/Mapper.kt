package hu.netacademia.androidhalado02.mapper

interface Mapper<T> {

    fun transformToStringList(target: List<T>): List<String>
}