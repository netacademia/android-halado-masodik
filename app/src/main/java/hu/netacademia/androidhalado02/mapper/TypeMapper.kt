package hu.netacademia.androidhalado02.mapper

import hu.netacademia.androidhalado02.entities.Type

class TypeMapper : Mapper<Type> {

    override fun transformToStringList(target: List<Type>): List<String> {
        return target.map { type ->
            type.name
        }
    }

    fun transformEngineTypeToStringList(target: List<Type>): List<String> {
        return target.map { type ->
            type.engineType
        }
    }
}