package hu.netacademia.androidhalado02.settings

import android.content.Context
import hu.netacademia.androidhalado02.R

class PreferenceConstants(context: Context) {

    val ORDER_BUTTON_ALLOWED = context.getString(R.string.preference_order_button)
}